function filterBy(arr, dataType) {
    let filteredArray = [];
    for (let i = 0; i < arr.length; i++) {
      const currentType = typeof arr[i];
      if (currentType !== dataType) {
        filteredArray.push(arr[i]);
      }
    }  
    return filteredArray;
  }
  const inputArray = ['hello', 'world', 23, '23', null];
  const resultArray = filterBy(inputArray, 'string');
  console.log(resultArray);
  